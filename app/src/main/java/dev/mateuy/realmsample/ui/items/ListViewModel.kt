package dev.mateuy.realmsample.ui.items

import android.os.Build
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import dev.mateuy.realmsample.ServiceLocator
import kotlinx.coroutines.flow.map
import java.time.LocalDateTime
import kotlin.random.Random

class ListViewModel : ViewModel() {
    val itemsDao = ServiceLocator.itemsDao
    val items = itemsDao.listFlow().asLiveData()

    fun insertItem(){
        val time = System.currentTimeMillis().toString()
        itemsDao.insertItem("Item at $time")
    }
}