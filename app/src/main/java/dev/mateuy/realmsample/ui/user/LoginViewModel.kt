package dev.mateuy.realmsample.ui.user

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dev.mateuy.realmsample.ServiceLocator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoginViewModel : ViewModel() {
    val loggedIn = MutableLiveData<Boolean>(false)

    fun register(email: String, password: String){
        CoroutineScope(Dispatchers.IO).launch{
            ServiceLocator.realmManager.register(email, password)
            loggedIn.postValue(true)
        }
    }

}